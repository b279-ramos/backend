let users = `[
  {
    "id": 1,
    "name": "Marvin Ramos",
    "username": "Marvs",
    "email": "marvin@ramos.com",
    "address": {
      "street": "Mabulo St.",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "09261123227",
    "website": "marvinramos.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 2,
    "name": "Marianne Ramos",
    "username": "yan",
    "email": "marianne@ramos.com",
    "address": {
      "street": "Mabulo St.",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-75.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "marianneramos.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 3,
    "name": "Jonas Ramos",
    "username": "Emboy",
    "email": "jonas@ramos.com",
    "address": {
      "street": "Mabulo St.",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-11.3159",
        "lng": "71.1496"
      }
    },
    "phone": "09324223227",
    "website": "jonasramos.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 4,
    "name": "wedilyn Ramos",
    "username": "yan",
    "email": "wedilyn@ramos.com",
    "address": {
      "street": "Mabulo St.",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-43.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "wedilynramos.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 5,
    "name": "Rosemarie Ramos",
    "username": "Rose",
    "email": "rosemarie@ramos.com",
    "address": {
      "street": "Mabulo St.",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "090998763227",
    "website": "rosemarieramos.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 6,
    "name": "Arden Ubaldo",
    "username": "kulot",
    "email": "arden@ubaldo.com",
    "address": {
      "street": "Prk. 1 ext. Bario Blaan Community Village",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-75.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "ardenubaldo.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 7,
    "name": "Roden Ubaldo",
    "username": "Kulot-2",
    "email": "roden@ubaldo.com",
    "address": {
      "street": "Prk. 1 ext. Bario Blaan Community Village",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-11.3159",
        "lng": "71.1496"
      }
    },
    "phone": "09324223227",
    "website": "rodenubaldo.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 8,
    "name": "Ivy Mae ubaldo",
    "username": "humay",
    "email": "ivymae@ubaldo.com",
    "address": {
      "street": "Prk. 1 ext. Bario Blaan Community Village",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-43.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "ivymaeubaldo.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 9,
    "name": "Jay ubaldo",
    "username": "humay",
    "email": "jay@ubaldo.com",
    "address": {
      "street": "Prk. 1 ext. Bario Blaan Community Village",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-43.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "jayubaldo.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  },{
    "id": 10,
    "name": "Jason ubaldo",
    "username": "gang-gang",
    "email": "jason@ubaldo.com",
    "address": {
      "street": "Prk. 1 ext. Bario Blaan Community Village",
      "suite": "Apt. 556",
      "city": "General Santos City",
      "zipcode": "9500",
      "geo": {
        "lat": "-43.3159",
        "lng": "11.1496"
      }
    },
    "phone": "0926114535556",
    "website": "jasonubaldo.org",
    "company": {
      "name": "Sample Company",
      "catchPhrase": "Sample company for everyone",
      "bs": "machine supply"
    }
  }
]`;

/*

1. The id property in the first user object is missing a value.
2. The website property in the first user object is missing a starting double quote.
3.  The company property in the third user object is missing an opening curly brace.
4. The website property in the fourth user object is missing a comma after its value.
5. The company property in the fourth user object is missing a closing curly brace.
6. The address property in the sixth user object is missing an opening curly brace.
7. The address property in the sixth user object is missing a closing curly brace.
8. The address property in the seventh user object is missing a closing curly brace.
9. The last curly brace at the end of the users string should be on a new line for better readability.

*/

let parsedUsers = JSON.parse(users)
console.log(parsedUsers);

let product = {
  name: 'Rusi Gala',
  category: 'Motorcycle',
  quantity: 293,
  model: 'rs123mmsa'
};

let stringifiedProduct = JSON.stringify(product)
console.log(stringifiedProduct);

try{
    module.exports = {

        parsedUsers: typeof parsedUsers !== 'undefined' ? parsedUsers : null,
        product: typeof product !== 'undefined' ? product : null,
        stringifiedProduct: typeof stringifiedProduct !== 'undefined' ? stringifiedProduct : null

    }
} catch(err){
}