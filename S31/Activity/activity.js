
// a. What directive is used by Node.js in loading the modules it needs?
Answer: require

// b. What Node.js module contains a method for server creation?
Answer: http

// c. What is the method of the http object responsible for creating a server using Node.js?
Answer: http.createServer()

// d. What method of the response object allows us to set status codes and content types?
Answer: writeHead()

// e. Where will console.log() output its contents when run in Node.js?
Answer: response.end()

// f. What property of the request object contains the address' endpoint?
Answer: request.url

const http = require("http");

// Create a variable "port" to store the port number.
const port = 4000;
const server = http.createServer((request, response) => {

    if(request.url == "/login") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello, Welcome to Login Page.");
	} else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("404: I'm sorry the page you are looking for cannot be found.");
	}
});

server.listen(port);
console.log(`Server now accessible at a port ${port}`);