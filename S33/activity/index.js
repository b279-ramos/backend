// #3
fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => console.log(data))

// #4
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then(data => {
		const titles = data.map(item => item.title);
		console.log(titles);
})

// #5
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(response => response.json())
	.then(data => console.log(data))

// #6
fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(response => response.json())
	.then(data => {
		const title = data.title;
		const status = data.completed ? 'completed' : 'not completed';
		console.log(`Title: ${title}, Status: ${status}`);
  })

// #7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
    title: 'New To-Do Item',
  })
})
	.then(response => response.json())
	.then(data => console.log(data))

// #8
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
    title: 'New To-Do Item',
  })
})
	.then(response => response.json())
	.then(data => console.log(data))

// #9
const todoId = 1; 
fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		title: 'Updated To-Do Item',
		description: 'This is an updated to-do item',
		status: 'in progress',
		dateCompleted: '2023-05-03',
		userId: 1
	})
})
	.then(response => response.json())
	.then(data => console.log(data))


// #10
fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
    	completed: true
  	})
})
	.then(response => response.json())
	.then(data => console.log(data))

// #11
fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
    	completed: true,
    	completedAt: new Date()
  })
})
	.then(response => response.json())
	.then(data => console.log(data))

// #12
fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`, {
	method: 'DELETE'
})