
// Set up depedencies.
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

// Server setup/middlewares
const app = express();
const port = 4000;

// Add task route.
// Allows all the task route created in the taskRoute.js file to use task route.

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// route must always below of this code
app.use("/tasks", taskRoute);

// connecting the database.
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.o4pg4os.mongodb.net/B279_to-do?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// server listening
if (require.main === module) {
    app.listen(port, () => console.log(`Server running at ${port}`));
}